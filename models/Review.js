const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const ReviewSchema = new Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: true,
    },
    place: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Place",
        required: true,
    },
    datetime: {
        type: String
    },
    comment: {
        type: String
    },
    qualityOfFood: {
        type: Number,
        required: true,
        default: 0
    },
    serviceQuality: {
        type: Number,
        required: true,
        default: 0
    },
    interior: {
        type: Number,
        required: true,
        default: 0
    },
});

const Review = mongoose.model("Review", ReviewSchema);

module.exports = Review;