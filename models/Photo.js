const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const PhotoSchema = new Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: true,
    },
    place: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Place",
        required: true,
    },
    image: {
        type: Array,
        required: true,
    },
    datetime: {
    type: String
}
});

const Photo = mongoose.model("Photo", PhotoSchema);

module.exports = Photo;
