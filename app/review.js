const express = require("express");
const auth = require("../middleware/auth");
const Review = require("../models/Review");

const router = express.Router();

router.post("/", auth, async (req, res) => {
    const reviewData = req.body;
    reviewData.user = req.user._id;
    reviewData.datetime = new Date();
    const review = new Review(reviewData);

    try {
        await review.save();
        return res.send({review});
    }catch (e) {
        res.status(400).send(e);
    }
});

router.delete("/:id", auth, async (req, res) => {
    try {
        const response = await Review.findByIdAndRemove(req.params.id);
        if (!response) {
            return res.sendStatus(404);
        }
        return res.send({
            message: `${req.params.id} removed`,
        });
    } catch (e) {
        return res.status(422).send(e);
    }
});

module.exports = router;
