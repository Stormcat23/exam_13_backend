const express = require("express");
const {nanoid} = require("nanoid");
const config = require("../config");
const multer = require("multer");
const path = require("path");
const auth = require("../middleware/auth");

const Photo = require("../models/Photo");

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get("/", async (req, res) => {
    let photos;
    try {
        if (req.query.place) {
            photos = await Photo.find({place: req.query.place}).sort({datetime: -1}).populate("user", "username");
        } else {
            photos = await Photo.find().populate("user", "username").sort({datetime: -1});
        }
        return res.send(photos);
    } catch (e) {
        return res.status(500).send(e);
    }
});

router.post("/", [auth, upload.array('image', 8)], async (req, res) => {
    const photoData = {
        user: req.body.user,
        place: req.body.place
    };

    const fileNames = req.files.map(item => {
        return item.filename
    })

    if (req.files) {
        photoData.image = fileNames;
    }

    photoData.datetime = new Date();

    const photo = new Photo(photoData);

    try {
        await photo.save();
        res.send(photo);
    } catch (e) {
        res.status(400).send(e);
    }
});


router.delete("/:id", auth, async (req, res) => {
    try {
        const response = await Photo.findByIdAndRemove(req.params.id);
        if (!response) {
            return res.sendStatus(404);
        }
        return res.send({
            message: `${req.params.id} removed`,
        });
    } catch (e) {
        return res.status(422).send(e);
    }
});

module.exports = router;
