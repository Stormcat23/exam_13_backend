const router = require("express").Router();
const multer = require("multer");
const path = require("path");
const {nanoid} = require("nanoid");
const config = require("../config");
const Place = require("../models/Place");
const User = require("../models/User");
const auth = require("../middleware/auth");
const mongoose = require('mongoose');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get("/", async (req, res) => {
    try {
        const places = await Place.aggregate([
            {$lookup: {from: "reviews", localField: "_id", foreignField: "place", as: "reviews"}},
            {$lookup: {from: "photos", localField: "_id", foreignField: "place", as: "photos"}},
            {$addFields: { totalScores : {
                        qualityOfFood: {$avg: "$reviews.qualityOfFood"},
                        serviceQuality: {$avg: "$reviews.serviceQuality"},
                        interior: {$avg: "$reviews.interior"},
                        overAll: {$avg:[
                            {$avg: "$reviews.qualityOfFood"},
                            {$avg: "$reviews.serviceQuality"},
                            {$avg: "$reviews.interior"}
                         ]},
                    }}},
        ])
        return res.send(places);
    } catch (e) {
        console.log(e)
        return res.status(500).send(e);
    }
});

router.get("/:id", async (req, res) => {
    try {
        const places = await Place.aggregate([
            {$match: {_id: mongoose.Types.ObjectId(req.params.id)}},
            {$lookup: {from: "reviews", localField: "_id", foreignField: "place", as: "reviews"}},
            {$lookup: {from: "photos", localField: "_id", foreignField: "place", as: "photos"}},
            {$addFields: { totalScores : {
                        qualityOfFood: {$avg: "$reviews.qualityOfFood"},
                        serviceQuality: {$avg: "$reviews.serviceQuality"},
                        interior: {$avg: "$reviews.interior"},
                        overAll: {$avg:[
                                {$avg: "$reviews.qualityOfFood"},
                                {$avg: "$reviews.serviceQuality"},
                                {$avg: "$reviews.interior"}
                            ]},
                    }}},
        ])
        return res.send(places);
        if (result) {
            res.send(result);
        } else {
            res.sendStatus(404);
        }
    } catch {
        res.sendStatus(500);
    }
});

router.post("/", upload.single("image"), auth, async (req, res) => {
    const token = req.get("Authorization");
    const user = await User.findOne({token});

    const placeData = req.body;
    placeData.user = user._id;
    placeData.datetime = new Date();

    if (req.file) {
        placeData.image = req.file.filename;
    }

    const place = new Place(placeData);
    try {
        await place.save();
        res.send(place);
    } catch (e) {
        res.status(400).send(e);
    }
});

module.exports = router;